const Pool = require('pg').Pool
const connectionString = 'postgres://postgres:P@ssW0rd!@localhost:5432/shop';
const pool = new Pool({
  connectionString: connectionString,
  idleTimeoutMillis: 3000,
  connectionTimeoutMillis: 2000
});

const sqlAdmin = `DROP TABLE IF EXISTS admin`;
const sqlCustomer = `DROP TABLE IF EXISTS customer`;
const sqlProduct = `DROP TABLE IF EXISTS product`;
const sqlCart = `DROP TABLE IF EXISTS cart`;
const sqlCheckout = `DROP TABLE IF EXISTS checkout`;

dropTable(sqlCustomer);

function dropTable (sql){
    pool.connect((err, client, release) => {
        if (err) {
          return console.error('Error acquiring client', err.stack)
        }
        client.query(sql , (err, result) => {
          release()
          if (err) {
            return console.error('Error executing query', err.stack)
          }
          console.log(result.rows)
        })
      })    
}
