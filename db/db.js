const { Pool } = require('pg');
const dotenv = require('dotenv');

dotenv.config();

const pool = new Pool({
  connectionString: process.env.DATABASE_URL
});


pool.on('connect', () => {
  console.log('connected to the db');
});

/**
 * Create Tables
 */
const createTables = () => {
  const queryText =
    `CREATE TABLE client (
      id serial,
      email varchar(100),
      pass varchar(100),
      PRIMARY KEY (id)
    );


    INSERT INTO client (email, pass) VALUES ('admin@gmail.com', '123456');
    
    CREATE TABLE product(
      id serial,
      product_name varchar(50),
      description varchar(150),
      price varchar(10),
      status varchar(20),
      PRIMARY KEY (id)
    );
    
    CREATE TABLE cart (
      id serial,
      client_id int NOT NULL,
      client_name varchar(100),
      address varchar(200),
      phone varchar(50),
      total varchar(50),
      FOREIGN KEY (client_id) REFERENCES client(id) ON DELETE CASCADE,
      PRIMARY KEY (id)
    );
    
    CREATE TABLE cart_product (
      cart_id int NOT NULL,
      product_id int NOT NULL,
      quantity int,
      FOREIGN KEY (cart_id) REFERENCES cart(id) ON DELETE CASCADE,
      FOREIGN KEY (product_id) REFERENCES product(id) ON DELETE CASCADE
    );`;


  pool.query(queryText)
    .then((res) => {
      console.log(res);
      pool.end();
    })
    .catch((err) => {
      console.log(err);
      pool.end();
    });
}


/**
 * Drop Tables
 */
const dropTables = () => {
  const queryText = 
  `
  DROP TABLE IF EXISTS cart_product; 
  DROP TABLE IF EXISTS cart;
  DROP TABLE IF EXISTS product;
  DROP TABLE IF EXISTS client;
  `;
  pool.query(queryText)
    .then((res) => {
      console.log(res);
      pool.end();
    })
    .catch((err) => {
      console.log(err);
      pool.end();
    });
}

pool.on('remove', () => {
  console.log('client removed');
  process.exit(0);
});

module.exports = {
  createTables,
  dropTables
};

require('make-runnable');