const Pool = require('pg').Pool
const connectionString = 'postgres://postgres:postgres@localhost:5432/shop';
const pool = new Pool({
    connectionString: connectionString
});

const sqlAdmin = `CREATE TABLE IF NOT EXISTS admin(
                  id SERIAL PRIMARY KEY,
                  email VARCHAR (50) UNIQUE NOT NULL,
                  password VARCHAR (50) NOT NULL
                  );

                  CREATE TABLE IF NOT EXISTS customer(
                  id SERIAL PRIMARY KEY,
                  email VARCHAR (50) UNIQUE NOT NULL,
                  password VARCHAR (300) NOT NULL,
                  phone VARCHAR (50) NOT NULL,
                  address VARCHAR (500) NOT NULL,
                  create_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP
                  );
                  CREATE TABLE IF NOT EXISTS product( 
                    id SERIAL PRIMARY KEY,
                    name VARCHAR (50) UNIQUE NOT NULL,
                    des VARCHAR (500) NOT NULL,
                    image_url VARCHAR (200) NOT NULL,
                    price INT NOT NULL,
                    sale INT NOT NULL,
                    create_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP
                    );
                  CREATE TABLE IF NOT EXISTS checkout(
                    id SERIAL PRIMARY KEY,
                    customer_id INT NOT NULL,
                    total INT NOT NULL,
                    phone VARCHAR(50) NOT NULL,
                    address VARCHAR(500) NOT NULL,
                    create_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
                    FOREIGN KEY (customer_id) REFERENCES customer(id)
                    );
                  CREATE TABLE IF NOT EXISTS cart(
                    id SERIAL PRIMARY KEY,
                    product_id INT NOT NULL,
                    customer_id INT NOT NULL,
                    checkout_id INT,
                    quantity INT NOT NULL,
                    status BOOLEAN NOT NULL,
                    FOREIGN KEY (customer_id) REFERENCES customer(id)
                    )`;


createTable(sqlAdmin);

function createTable(sql) {
    pool.connect((err, client, release) => {
        if (err) {
            return console.error('Error acquiring client', err.stack)
        }
        client.query(sql, (err, result) => {
            release()
            if (err) {
                return console.error('Error executing query', err.stack)
            }
            console.log(result)
        })
    })
}



`
customer: Tạo chức năng đăng ký, đăng nhập
    - id: SERIAL PRIMARY KEY
    - email: VARCHAR(50) UNIQUE
    - password: VARCHAR(50)
    - phonenumber: VARCHAR(50)
    - addr: VARCHAR(500)

admin: tự thêm sẵn trong CSDL
    - id: SERIAL PRIMARY KEY
    - email: VARCHAR(50)
    - password: VARCHAR(50)
    - createAt:  DATE

product:
    - id: SERIAL PRIMARY KEY
    - name: VARCHAR(50)
    - mô tả: TEXT
    - imageURL: VARCHAR(200)
    - giá tiền: INT
    - sale: INT
    - createAt: DATE

    10/4: mua 2 san phẩm -> đã checkout 
    26/3: mua them 3 

cart: // mua 3 sp => 3 carts => tham chieu id vao checkout
'CONSTRAINT' trong SQL 
    - id: SERIAL PRIMARY KEY
    - customer id -> người mua hàng: INT , FOREIGN KEY (customer_id) REFERENCES customer(id),
    - checkoutId: INT FOREIGN KEY (cart_id) REFERENCES cart(id),
    - sản phẩm -> id sản phẩm : INT
    - số lượng: INT
    - status -> da thanh toan hay chua: BOOL

bấm checkout -> lấy thông tin sản phẩm  chưa checkout -> giao diện trang check oout -> tạo checkout -> update cart_checkout

checkout: 3 sản phẩm -> 3 cardId
    - id: SERIAL PRIMARY KEY
    - customer id: INT
    - giá thành: INT
    - giá thành sau sale: INT
    - sdt: VARCHAR(200)
    - địa chỉ người mưa hàng:  VARCHAR(500)
    - creatAt: DATE
`