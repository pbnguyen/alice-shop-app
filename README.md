# Alice Shop App

## Set up
1. npm i
2. Chỉnh lại connectionString trong file ./db/createDB.js và ./db/createTables.js
2. node ./db/createDB.js
3. node ./db/createTables.js
4. npm start

## Thông tin API admin

### Product
1. Create

request:

    ```
        url: /api/product/create
        method: post
        params:
            {
                "name":   tên sản phẩm,
                "des": Mô tả sản phẩm,
                "image_url": link ảnh sản phẩm,
                "price": giá sản phẩm,
                "sale":  % sale
            }
    ```
respone
Nếu thành công
```
    {
        'success': true,
        'msg': 'Create Product succes',
        'data': params in request
    }
```         
Nếu lỗi
```
    {
        'success': false,
        'msg': Thông báo lỗi,
        'data': []
    }
``` 
2. Delete
request
    ```
        url: /api/product/delete
        method: post
        params:
            {
                "id"
            }
    ```
respone

3. Update

    ```
        url: /api/product/update
        method: post
        params:
            {
                "name":   tên sản phẩm,
                "des": Mô tả sản phẩm,
                "image_url": link ảnh sản phẩm,
                "price": giá sản phẩm,
                "sale":  % sale
            }
    ```

4. List All

    ```
        url: /api/product/listall
        method: get
    ```

5. Get Infor
    ```
        url: /api/product/listall
        method: get
        params: 
        {
            "id": product id
        }
    ```
6. Upload Image
### request
    ```
        url: /api/product/uploadimage
        method: post
        params:
            image
    ```
### respone:
Nếu lỗi
    ```
        'success': false,
        'msg': 'Upload Image Failed',
        'data': ''
    ```
Nếu thành công
    ```
        {
            'success': true,
            'msg': 'Upload Image Success',
            'data': url ảnh
        }  
    ```

