// express
const express = require('express');
const app = express();
const path = require('path');

const clientRoute = require('./routers/client')
const productRoute = require('./routers/product')
const cartRoute = require('./routers/cart')
const productAPI = require('./routers/api/product');
const customerAPI = require('./routers/api/customer');
const cartApi = require('./routers/api/cart');

const port = 3000


// body-parser
const bodyParser = require('body-parser');
app.use(bodyParser.json())
app.use(
    bodyParser.urlencoded({
        extended: true,
    })
)

// ejs
app.use(express.static('public'));
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs')



app.get('/', function(req, res) {
    res.render('index');
})

app.get('/index', function(req, res) {
    res.render('index');
})

app.get('/about', function(req, res) {
    res.render('about');
})

app.get('/flower', function(req, res) {
    res.render('flower');
})

app.get('/plant', function(req, res) {
    res.render('plant');
})

app.get('/basket', function(req, res) {
    res.render('basket');
})

app.get('/login', function(req, res) {
    res.render('login');
})

app.get('/signup', function(req, res) {
    res.render('signup')
})

app.get('/cart', function(req, res) {
    res.render('cart');
})

app.get('/history', function(req, res) {
    res.render('history');
})

app.get('/admin', function(req, res) {
    res.render('./admin/admin');
})

app.get('/admin/add_product', function(req, res) {
    res.render('./admin/admin_add_product');
})

// // process.env
// var port = process.env.PORT || 3000;
// var user = process.env.RDS_USERNAME || 'postgres';
// var host = process.env.RDS_HOSTNAME || 'localhost';
// var password = process.env.RDS_PASSWORD || 'Alicedep1';
// var portDB = process.env.RDS_PORT || 5432;

// // pg
// const Pool = require('pg').Pool;
// const pool = new Pool({
//     user: user,
//     host: host,
//     database: 'Database',
//     password: password,
//     port: portDB,
// });

// routes
app.use('/clients', clientRoute);
app.use('/products', productRoute)
app.use('/carts', cartRoute)

// Alice API
app.use('/api/product', productAPI);
app.use('/api/cart', cartApi);
app.use('/api/customer', customerAPI);

app.listen(port, () => {
    console.log(`Server running at http://127.0.0.1:3000 or App running on port ${port}.`)
})