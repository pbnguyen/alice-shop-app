const express = require('express');
var router = express.Router();

const { Pool } = require('pg');
const dotenv = require('dotenv');
const { check, validationResult } = require('express-validator');
const bcrypt = require('bcrypt');
const saltRounds = 10;

dotenv.config();

const pool = new Pool({
    connectionString: process.env.DATABASE_URL
});

router.post('/create', [
    check('email', 'email is not valid').isEmail(),
    check('email', 'email is required').not().isEmpty(),
    check('pass', 'password is required').not().isEmpty(),
    check('pass', 'password must at least 6 characters').isLength({ min: 6 }),
    check('confirm', 'confirm password is required').not().isEmpty(),
    check('pass').custom((value, { req }) => {
        if (value !== req.body.confirm) {
            throw new Error("Passwords don't match");
        } else {
            return true;
        }
    })
], (request, response) => {
    const { email, pass, confirm } = request.body

    //check validate data
    const result = validationResult(request);
    var errors = result.errors;

    if (!result.isEmpty()) {
        //response validate data to register.ejs
        response.render('signup', {
            errors: errors,
            email: email,
            pass: pass,
            confirm: confirm,
        })
    } else {
        pool.query('SELECT * FROM client WHERE email = $1', [email], (error, results) => {
            if (error) {
                throw error
            } else {
                if (results.rowCount != 0) {
                    response.render('signup', {
                        email_used: "Email already in use",
                        email: email,
                        pass: pass,
                        confirm: confirm,
                    })
                    console.log("DUPLICATE ")
                    // response.redirect('/signup?validEmail=F')
                } else {
                    console.log("OK TO INSERT")
                    bcrypt.hash(pass, saltRounds, (err, hash) => {
                        // Now we can store the password hash in db.
                        pool.query('INSERT INTO client (email, pass) VALUES ($1,$2)', [email, hash], (error, results) => {
                            if (error) {
                                throw error
                            }
                            response.redirect('/login?newAccount=T')
                        })
                    });
                }
            }
        })
    }
});



router.post('/login', (request, response) => {
    const { email, pass } = request.body
        pool.query('SELECT id, email, pass FROM client WHERE email = $1', [email], (error, results) => {
            if (error) {
                throw error
            } else { 
                if (results.rowCount == 0) {
                    response.render('login', {
                    no_client: "Account with this email does not exist",
                    email: email,
                    pass: pass
                    })    
                }
                else {
                    bcrypt.compare(pass, results.rows[0].pass, function (err, res) {
                        if (res == true) {
                            // response.render('index', {
                            // client_id: results.rows[0].id,
                            // })
                            let client_id = results.rows[0].id
                            let client_email = results.rows[0].email
                            response.redirect('/index?client_id=' + client_id +'&client_email=' + client_email)
                        } else {
                            response.render('login', {
                            wrong: "password is not correct",
                            email: email,
                            pass: pass
                            })
                        }
                    });
                }
            }
        })
    })

        
        //     pool.query('SELECT * FROM client WHERE email = $1 and pass = $2', [email, pass], (error, results) => {
        //         if (error) {
        //             throw error
        //         } else {
        //             if (results.rowCount == 0) {
        //                 pool.query('SELECT * FROM client WHERE email = $1 or pass = $2', [email, pass], (error, results) => {
        //                     if (error) {
        //                         throw error
        //                     } else {
        //                         if (results.rowCount == 0) {
        //                             console.log("NO ACCOUNT")
        //                             response.render('login', {
        //                                 no_client: "Account with this email does not exist",
        //                             })
        //                             // response.redirect('/login?validAccount=N')
        //                         } else {
        //                             console.log("WRONG")
        //                             response.render('login', {
        //                                 wrong: "Email or password is not correct",
        //                             })
        //                             // response.redirect('/login?validAccount=W')
        //                         }
        //                     }
        //                 })
        //             } else {
        //                 response.render('index', {
        //                     client_id: results.rows[0].id,
        //                 })
        //                 // let client_id = results.rows[0].id
        //                 // response.redirect('/index?client_id=' + client_id)
        //             }
        //         }
        //         // response.status(200).json(results.rows)
        //     })
    // }
// });

module.exports = router;



