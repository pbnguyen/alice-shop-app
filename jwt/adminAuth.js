/**
 * File: /Users/phanbaonguyen/Documents/Alice/alice-shop-app/jwt/adminAuth.js
 * Project: /Users/phanbaonguyen/Documents/Alice/alice-shop-app
 * Created Date: Monday, April 27th 2020, 9:23:46 pm
 * Author: Invoker
 * -----
 * Last Modified: Mon Apr 27 2020
 * Modified By: Invoker
 * -----
 * Copyright (c) 2020 Pyroject
 * ------------------------------------
 * Javascript will save your soul!
 */
/**
 * File: /Users/phanbaonguyen/Documents/Alice/alice-shop-app/jwt/customerAuth.js
 * Project: /Users/phanbaonguyen/Documents/Alice/alice-shop-app
 * Created Date: Monday, April 27th 2020, 8:45:19 pm
 * Author: Invoker
 * -----
 * Last Modified: Mon Apr 27 2020
 * Modified By: Invoker
 * -----
 * Copyright (c) 2020 Pyroject
 * ------------------------------------
 * Javascript will save your soul!
 */
const jwt = require('jsonwebtoken');
const Joi = require('@hapi/joi');
const { Pool } = require('pg');
const dotenv = require('dotenv');
const bcrypt = require('bcrypt');

dotenv.config();
const pool = new Pool({
    connectionString: process.env.DATABASE_URL
});


const adminSignIn = (req,res,next) =>{
    const {email,password} = req.body;
    const schema  = Joi.object({
        email: Joi.string().required(),
        password: Joi.string().required()
    })
   const {err, value} = schema.validate( {email,password} );
   if (err){
        res.send({
            'success': false,
            'msg': 'Params Error!',
            'data': []
        })
   } 
   else {
        const sqlFindUser = 'SELECT * FROM admin WHERE email = $1';
        pool.query(sqlFindUser,[value.email],(err,result)=>{
            if (err){
                res.send({
                    'success': false,
                    'msg': 'Server Error!',
                    'data': []
                })
            }
            else {
                if (result.rows[0]){
                    bcrypt.compare(value.password, result.rows[0].password, function (err, resBcrypt) {
                        if (resBcrypt == true) {
                            const token = jwt.sign({ id: result.rows[0].id ,role: 'admin' }, process.env.JWT_KEY );
                            res.send({
                                'success': true,
                                'msg': 'Signin Success',
                                'data': token
                            })
                        } else {
                            res.send({
                                'success': false,
                                'msg': 'Password is Wrong!',
                                'data': []
                            })
                        }
                    });
                }
                else {
                    res.send({
                        'success': false,
                        'msg': 'Email Not Exists!',
                        'data': []
                    })
                }
            }
        })
   }
}  
const isAdminSignedIn = (req,res,next)=>{
    const tokenRaw = req.headers.authorization;
    if (!tokenRaw){
        res.send({
            'success': false,
            'msg': 'Not Signin',
            'data': []
        })
    }
    else {
        const token = tokenRaw.replace('Bearer ','')
        jwt.verify(token, process.env.JWT_KEY , function(err, decoded) {
            if (err){
                res.send({
                    'success': false,
                    'msg': 'Not Signin',
                    'data': []
                })  
            }
            else {
                const role = decoded.role || '';
                const id   = decoded.id || 0;
                if (role === "admin"){
                    req.user = {id,role}
                    next();
                }
                else {
                    res.send({
                        'success': false,
                        'msg': 'Not Signin',
                        'data': []
                    })  
                }
            }
        });
    }
}
module.exports = {adminSignIn,isAdminSignedIn};