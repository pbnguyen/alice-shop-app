const express = require('express');
var router = express.Router();

const { Pool } = require('pg');
const dotenv = require('dotenv');
const bcrypt = require('bcrypt');
const saltRounds = 10;

const bodyParser = require('body-parser');
router.use(bodyParser.json())
router.use(
    bodyParser.urlencoded({
        extended: true,
    })
)

dotenv.config();

const pool = new Pool({
    connectionString: process.env.DATABASE_URL
});

// create new account
router.post('/create', (request, response) => {
    const email = request.body.email
    const pass = request.body.pass

    bcrypt.hash(pass, saltRounds, (err, hash) => {
        pool.query('INSERT INTO client (email, pass) VALUES ($1, $2)', [email, hash], (error, results) => {
            if (error) {
                throw error
            }
        })
        response.status(200).send({"res": "Client Added"})
    });
});

// get client for auto login
router.post('/getClientByEmail', (request, response) => {
    const email = request.body.email;
    console.log(email)
    pool.query('SELECT * FROM client WHERE email = $1', [email], (error, results) => {
        if (error) {
            throw error
        }
        response.status(200).json(results.rows)
    })
});


// login by email and pass
router.post('/getClientByEmailAndPass', (request, response) => {
    const email = request.body.email;
    const pass = request.body.pass;

    if (email == 'admin@gmail.com') {
        pool.query('SELECT * FROM client WHERE email = $1 AND pass = $2', [email, pass], (error, results) => {
            if (error) {
                throw error
            }
            response.status(200).json(results.rows)
        });
    } else {
        pool.query('SELECT * FROM client WHERE email = $1', [email], (error, results) => {
            if (error) {
                throw error
            }
            if (results.rowCount != 0) {
                bcrypt.compare(pass, results.rows[0].pass, function (err, res) {
                    if (res == true) {
                        response.status(200).json(results.rows)
                    } else {
                        response.status(200).json([])
                    }
                });
            } else {
                response.status(200).json([])
            }
        });
    }
});



module.exports = router;

