const express = require('express');
var router = express.Router();

const { Pool } = require('pg');
const dotenv = require('dotenv');
const { check, validationResult } = require('express-validator');


dotenv.config();

const pool = new Pool({
    connectionString: process.env.DATABASE_URL
});


// PRODUCT

// create product
router.post('/createProduct', (request, response) => {
    const { product_name, description, price, status } = request.body
    pool.query('INSERT INTO product (product_name, description, price, status) VALUES ($1, $2, $3, $4)', [product_name, description, price, status], (error, results) => {
        if (error) {
            throw error
        }
        response.status(201).send(`Success add new product`)
    })
})


// Get all product
router.get('/getAll', (request, response) => {
    pool.query('SELECT * FROM product ORDER BY name ASC', (error, results) => {
        if (error) {
            throw error
        }
        response.status(200).json(results.rows)
    })
})

// Delete product
router.delete('/deleteProduct', (request, response) => {
    const product_name = request.query.product_name;
    console.log(product_name)
    pool.query('DELETE from product WHERE product_name = $1', [product_name], (error, results) => {
        if (error) {
            throw error
        }
        response.status(201).send(`Deleted product`)
    })
})

module.exports = router;