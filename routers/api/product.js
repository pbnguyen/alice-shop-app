/**
 * File: /Users/phanbaonguyen/Documents/Alice/s3634707_A2 2/routers/api/product.js
 * Project: /Users/phanbaonguyen/Documents/Alice/s3634707_A2 2
 * Created Date: Sunday, April 26th 2020, 8:58:27 pm
 * Author: Invoker
 * -----
 * Last Modified: Mon Apr 27 2020
 * Modified By: Invoker
 * -----
 * Copyright (c) 2020 Pyroject
 * ------------------------------------
 * Javascript will save your soul!
 */
const express = require('express');
const Joi = require('@hapi/joi');
const path = require('path');
const imagePath = path.join(__dirname, '../../public/image/upload/');

var fs = require('fs');
var formidable = require('formidable');

var router = express.Router();

const { Pool } = require('pg');
const dotenv = require('dotenv');
const { check, validationResult } = require('express-validator');
const { adminSignIn, isAdminSignedIn } = require('../../jwt/adminAuth');

dotenv.config();

const pool = new Pool({
    connectionString: process.env.DATABASE_URL
});

// API Product:
//  - createProduct
//  - deleteProduct: delete by Id
//  - updateProduct: checkProduct exists -> update
//  - listAllProduct
//  - getProduct: get by Id
//  - uploadImageProduct

router.post('/create', (req, res) => {
    const { name, des, image_url, price, sale } = req.body;
    const schema = Joi.object({
        name: Joi.string().required(),
        des: Joi.string().required(),
        image_url: Joi.string().required(),
        price: Joi.number().integer().required(),
        sale: Joi.number().integer().min(0).max(100).required(),
    });
    const { error, value } = schema.validate({ name, des, image_url, price, sale });
    if (error) {
        console.log(error);
        res.send({
            'success': false,
            'msg': 'Params Error!',
            'data': []
        })
    } else {
        const findProduct = `SELECT * FROM product WHERE name = $1`;
        pool.query(findProduct, [value.name], (err, result) => {
            if (err) {
                console.log(err);
                res.send({
                    'success': false,
                    'msg': 'Server Error!',
                    'data': []
                })
            } else {
                if (result.rows[0]) {
                    res.send({
                        'success': false,
                        'msg': 'Product name exists',
                        'data': []
                    })
                } else {
                    const sql = `INSERT INTO product (name,des,image_url,price,sale)
                    VALUES ($1,$2,$3,$4,$5)`
                    pool.query(sql, [value.name, value.des, value.image_url, value.price, value.sale], (err, result) => {
                        if (err) {
                            console.log(err);
                            res.send({
                                'success': false,
                                'msg': 'Server Error!',
                                'data': []
                            })
                        } else {
                            res.send({
                                'success': true,
                                'msg': 'Create Product success',
                                'data': value
                            })
                        }
                    })
                }
            }
        })

    }
})

router.get('/listall', (req, res) => {
    const sql = `SELECT * FROM product`;
    pool.query(sql, (err, result) => {
        if (err) {
            console.log(error);
            res.send({
                'success': false,
                'msg': 'Server Error!',
                'data': []
            })
        } else {
            if (result.rowCount > 0) {
                res.send({
                    'success': true,
                    'msg': '',
                    'data': result.rows
                })
            } else {
                res.send({
                    'success': true,
                    'msg': 'No product',
                    'data': []
                })
            }
        }
    })
})
router.get('/getbyid', (req, res) => {
    const { id } = req.body;
    const sql = `SELECT * FROM product WHERE id = $1`;
    pool.query(sql, [id], (err, result) => {
        if (err) {
            console.log(error);
            res.send({
                'success': false,
                'msg': 'Server Error!',
                'data': []
            })
        } else {
            console.log(result.rows);
            if (result.rows[0]) {
                res.send({
                    'success': true,
                    'msg': 'Get 1 product',
                    'data': result.rows[0]
                })
            } else {
                res.send({
                    'success': true,
                    'msg': 'Product does not exist',
                    'data': []
                })
            }
        }
    })
})



router.post('/delete', isAdminSignedIn, (req, res) => {
    const { id } = req.body;
    const schema = Joi.object({
        id: Joi.number().integer().required(),
    });

    const { error, value } = schema.validate({ id });
    if (error) {
        console.log(error);
        res.send({
            'success': false,
            'msg': 'Params Error!',
            'data': []
        })
    } else {
        const deleteProduct = "DELETE from product WHERE id = $1"
        pool.query(deleteProduct, [id], (err, result) => {
            if (err) {
                console.log(error);
                res.send({
                    'success': false,
                    'msg': 'Server Error!',
                    'data': []
                })
            } else {
                res.send({
                    'success': true,
                    'msg': 'Delete success',
                    'data': []
                })
            }
        })
    }
})


router.post('/update', (req, res) => {
    const { id, name, des, image_url, price, sale } = req.body;
    const schema = Joi.object({
        id: Joi.number().integer().required(),
        name: Joi.string().required(),
        des: Joi.string().required(),
        image_url: Joi.string().required(),
        price: Joi.number().integer().required(),
        sale: Joi.number().integer().min(0).max(100).required(),
    });

    const { error, value } = schema.validate({ id, name, des, image_url, price, sale });
    if (error) {
        console.log(error);
        res.send({
            'success': false,
            'msg': 'Params Error!',
            'data': []
        })
    } else {
        const findProductName = `SELECT * FROM product WHERE name = $1`;
        pool.query(findProductName, [value.name], (err, result) => {
            if (err) {
                console.log(err);
                res.send({
                    'success': false,
                    'msg': 'Server Error!',
                    'data': []
                })
            } else {
                if (result.rows[0]) {
                    if (value.id === result.rows[0].id) {
                        const sql = `UPDATE product
                            SET name = $2, des = $3, image_url = $4, price = $5, sale= $6  WHERE id = $1`
                        pool.query(sql, [id, value.name, value.des, value.image_url, value.price, value.sale], (err, result) => {
                            if (err) {
                                console.log(err);
                                res.send({
                                    'success': false,
                                    'msg': 'Server Error!',
                                    'data': []
                                })
                            } else {
                                res.send({
                                    'success': true,
                                    'msg': 'Update Product success',
                                    'data': result.rows
                                })
                            }
                        })
                    } else {
                        res.send({
                            'success': false,
                            'msg': 'Product name exists',
                            'data': []
                        })
                    }
                } else {

                    const sql = `UPDATE product
                        SET name = $2, des = $3, image_url = $4, price = $5, sale= $6  WHERE id = $1`
                    pool.query(sql, [id, value.name, value.des, value.image_url, value.price, value.sale], (err, result) => {
                        if (err) {
                            console.log(err);
                            res.send({
                                'success': false,
                                'msg': 'Server Error!',
                                'data': []
                            })
                        } else {
                            res.send({
                                'success': true,
                                'msg': 'Update Product success',
                                'data': result.rows
                            })
                        }
                    })
                }
            }
        })
    }
})

router.post('/uploadimage', (req, res) => {
    var form = new formidable.IncomingForm();
    form.parse(req, function(err, fields, file) {
        var path = file.file.path;
        let newPath = imagePath + file.file.name;
        fs.rename(path, newPath, function(err) {
            if (err) {
                console.log(err);
                res.send({
                    'success': false,
                    'msg': 'Upload Image Failed',
                    'data': ''
                })
            } else {
                url = '/image/upload/' + file.files.name;
                res.send({
                    'success': true,
                    'msg': 'Upload Image Success',
                    'data': url
                })
            }
        });
    });
})

module.exports = router;