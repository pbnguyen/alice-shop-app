/**
 * File: /Users/phanbaonguyen/Documents/Alice/s3634707_A2 2/routers/api/cart.js
 * Project: /Users/phanbaonguyen/Documents/Alice/s3634707_A2 2
 * Created Date: Monday, April 27th 2020, 10:03:34 am
 * Author: Invoker
 * -----
 * Last Modified: Mon Apr 27 2020
 * Modified By: Invoker
 * -----
 * Copyright (c) 2020 Pyroject
 * ------------------------------------
 * Javascript will save your soul!
 */

/***********
 API Cart
    1. Creat Cart: Tạo Cart
        url: /api/cart/create
        method: post
    2. Delete Cart: Xoá Cart bởi Id
        url: /api/cart/delete
        method: post
    3. Update Cart: Chỉnh sửa Cart (số lượng,...)
        url: /api/cart/update
        method: post
    4. Get Carts by User Id:  Lấy Carts dựa trên User Id (những Cart chưa checkout)
        url: /api/cart/getcartbyuser
        method: post
    5. Get Cart by Cart Id: Lấy thông tin cart dựa trên Id của Cart
        url: /api/cart/getcart
        method: get
    6. Get Carts by Checkout Id: Lấy Carts dựa trên Checkout Id ( những Cart đã được thanh toán)
        url: /api/cart/getcartbycheckout
        method: get
*************/

const express = require('express');
const Joi = require('@hapi/joi');

var router = express.Router();

const { Pool } = require('pg');
const dotenv = require('dotenv');
const {customerSignIn,isSignedIn} = require('../../jwt/customerAuth')
dotenv.config();

const pool = new Pool({
    connectionString: process.env.DATABASE_URL
});

router.post('/create', isSignedIn, (req, res) => {

    const { product_id, quantity } = req.body;
    const customer_id = req.user.id;

    const schema = Joi.object({
        customer_id: Joi.number().integer().required(),
        product_id: Joi.number().integer().required(),
        quantity: Joi.number().integer().min(1).required(),
    });

    const { error, value } = schema.validate({ customer_id, product_id, quantity });
    if (error) {
        console.log(error);
        res.send({
            'success': false,
            'msg': 'Params Error!',
            'data': []
        })
    }
    else {
        // find cart for that product and customer with status
        const sql = `SELECT * from cart WHERE customer_id = $1 AND product_id = $2 AND status = $3`
        pool.query(sql, [value.customer_id, value.product_id, 'FALSE'], (err, result) => {
            if (err) {
                console.log(err);
                res.send({
                    'success': false,
                    'msg': 'Server Error!',
                    'data': []
                })
            }
            else {
                if (result.rows[0]) {
                    // can not add new cart with this product for this customer
                    res.send({
                        'success': false,
                        'msg': 'Customer had a cart with this product!',
                        'data': []
                    })
                } else {
                    // add new cart 
                    const sql = `INSERT INTO cart (customer_id, product_id, quantity, status)
                                        VALUES ($1,$2,$3,$4)`
                    pool.query(sql, [value.customer_id, value.product_id, value.quantity, 'FALSE'], (err, result) => {
                        if (err) {
                            console.log(err);
                            res.send({
                                'success': false,
                                'msg': 'Server Error!',
                                'data': []
                            })
                        }
                        else {
                            res.send({
                                'success': true,
                                'msg': 'Create Cart success',
                                'data': value
                            })
                        }
                    })
                }
            }
        })
    }
})


router.post('/delete', isSignedIn, (req, res) => {
    const { id } = req.body;
    const schema = Joi.object({
        id: Joi.number().integer().required(),
    });

    const { error, value } = schema.validate({ id });
    if (error) {
        console.log(error);
        res.send({
            'success': false,
            'msg': 'Params Error!',
            'data': []
        })
    }
    else {
        const deleteProduct = "DELETE from product WHERE id = $1"
        pool.query(deleteProduct, [id], (err, result) => {
            if (err) {
                console.log(error);
                res.send({
                    'success': false,
                    'msg': 'Server Error!',
                    'data': []
                })
            }
            else {
                res.send({
                    'success': true,
                    'msg': 'Delete success',
                    'data': []
                })
            }
        })
    }
})

router.post('/updatequantity',isSignedIn, (req, res) => {
    const { id, quantity } = req.body;

    const schema = Joi.object({
        id: Joi.number().integer().required(),
        quantity: Joi.number().integer().required(),
    });

    const { error, value } = schema.validate({ id, quantity });
    if (error) {
        console.log(error);
        res.send({
            'success': false,
            'msg': 'Params Error!',
            'data': []
        })
    }
    else {
        const findCart = `SELECT * FROM cart WHERE id = $1`;
        pool.query(findCart, [value.id], (err, result) => {
            if (err) {
                console.log(err);
                res.send({
                    'success': false,
                    'msg': 'Server Error!',
                    'data': []
                })
            }
            else {
                const sql = `UPDATE cart SET quantity = $1 WHERE id = $2`
                pool.query(sql, [value.quantity, value.id], (err, result) => {
                    if (err) {
                        console.log(err);
                        res.send({
                            'success': false,
                            'msg': 'Server Error!',
                            'data': []
                        })
                    }
                    else {
                        res.send({
                            'success': true,
                            'msg': 'Update Cart quantity success!',
                            'data': result.rows
                        })
                    }
                })
            }
        })
    }
})


router.post('/updatecheckout', (req, res) => {
    const { id, checkout_id } = req.body;

    const schema = Joi.object({
        id: Joi.number().integer().required(),
        checkout_id: Joi.number().integer().required(),
    });

    const { error, value } = schema.validate({ id, checkout_id });
    if (error) {
        console.log(error);
        res.send({
            'success': false,
            'msg': 'Params Error!',
            'data': []
        })
    }
    else {
        const findCheckout = `SELECT * FROM checkout WHERE id = $1`;
        pool.query(findCheckout, [value.checkout_id], (err, result) => {
            if (err) {
                console.log(err);
                res.send({
                    'success': false,
                    'msg': 'Server Error!',
                    'data': []
                })
            } else {
                const sql = `UPDATE cart SET checkout_id = $1, status = 'TRUE' WHERE id = $2`
                pool.query(sql, [value.checkout_id, value.id], (err, result) => {
                    if (err) {
                        console.log(err);
                        res.send({
                            'success': false,
                            'msg': 'Server Error!',
                            'data': []
                        })
                    }
                    else {
                        res.send({
                            'success': true,
                            'msg': 'Update Cart Checkout success!',
                            'data': result.rows
                        })
                    }
                })
            }
        })
    }
})


//  pending cart
router.get('/getcartbyuser', (req, res) => {
    const { customer_id } = req.body;

    const schema = Joi.object({
        customer_id: Joi.number().integer().required(),
    });

    const { error, value } = schema.validate({ customer_id });
    if (error) {
        console.log(error);
        res.send({
            'success': false,
            'msg': 'Params Error!',
            'data': []
        })
    }
    else {
        const sql = `SELECT * from cart WHERE customer_id = $1 AND status = 'false' `
        pool.query(sql, [value.customer_id], (err, result) => {
            if (err) {
                console.log(err);
                res.send({
                    'success': false,
                    'msg': 'Server Error!',
                    'data': []
                })
            }
            else {
                console.log(result.rows);
                res.send({
                    'success': true,
                    'msg': 'Get unpaid cart!',
                    'data': result.rows
                })
            }
        })       
    }
})



router.get('/getcart', (req, res) => {
    const { id } = req.body;

    const schema = Joi.object({
        id: Joi.number().integer().required(),
    });

    const { error, value } = schema.validate({ id });
    if (error) {
        console.log(error);
        res.send({
            'success': false,
            'msg': 'Params Error!',
            'data': []
        })
    }
    else {
        const sql = `SELECT * from cart WHERE id = $1`
        pool.query(sql, [value.id], (err, result) => {
            if (err) {
                console.log(err);
                res.send({
                    'success': false,
                    'msg': 'Server Error!',
                    'data': []
                })
            }
            else {
                console.log(result.rows);
                res.send({
                    'success': true,
                    'msg': '',
                    'data': result.rows
                })
            }
        })       
    }
})


// paid cart
router.get('/getcartbycheckout', (req, res) => {
    const { checkout_id } = req.body;

    const schema = Joi.object({
        checkout_id : Joi.number().integer().required(),
    });

    const { error, value } = schema.validate({ checkout_id  });
    if (error) {
        console.log(error);
        res.send({
            'success': false,
            'msg': 'Params Error!',
            'data': []
        })
    }
    else {
        const sql = `SELECT * from cart WHERE checkout_id = $1`
        pool.query(sql, [value.checkout_id], (err, result) => {
            if (err) {
                console.log(err);
                res.send({
                    'success': false,
                    'msg': 'Server Error!',
                    'data': []
                })
            }
            else {
                console.log(result.rows);
                res.send({
                    'success': true,
                    'msg': '',
                    'data': result.rows
                })
            }
        })       
    }
})


module.exports = router;