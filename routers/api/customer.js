/**
 * File: /Users/phanbaonguyen/Documents/Alice/s3634707_A2 2/routers/api/customer.js
 * Project: /Users/phanbaonguyen/Documents/Alice/s3634707_A2 2
 * Created Date: Monday, April 27th 2020, 10:05:34 am
 * Author: Invoker
 * -----
 * Last Modified: Mon Apr 27 2020
 * Modified By: Invoker
 * -----
 * Copyright (c) 2020 Pyroject
 * ------------------------------------
 * Javascript will save your soul!
 */

/* API Customer
    1. signin
    2. signup
    3. signout -> frontend
*/
const express = require('express');
const Joi = require('@hapi/joi');
var router = express.Router();
const { Pool } = require('pg');
const dotenv = require('dotenv');
const bcrypt = require('bcrypt');
const saltRounds = 10;
const  {customerSignIn,isSignedIn}  =require('../../jwt/customerAuth');

dotenv.config();
const pool = new Pool({
    connectionString: process.env.DATABASE_URL
});

router.post('/signin', customerSignIn, (req,res) =>{

})
router.get('/aboutme', isSignedIn, (req,res)=>{
    const user = req.user;
    res.send({user});
})

router.post('/signup',(req,res) =>{
    const { email,password,phone,address } = req.body;
    const schema = Joi.object({
        email: Joi.string().required(),
        password: Joi.string().required(),
        phone: Joi.string().required(),
        address: Joi.string().required()
    })
    const {error,value} = schema.validate({ email,password,phone,address });
    if (error) {
        console.log(error);
        res.send({
            'success': false,
            'msg': 'Params Error!',
            'data': []
        })
    }
    else {
        const sqlFindOne = 'SELECT * FROM customer WHERE email = $1'; 
        pool.query(sqlFindOne,[value.email],(err,result)=>{
            if (err){
                console.log(err);
                res.send({
                    'success': false,
                    'msg': 'Server Error!',
                    'data': []
                })
            }
            else {
                if (result.rows[0]){
                    res.send({
                        'success': false,
                        'msg': 'User exists',
                        'data': []
                    })
                }
                else {
                    bcrypt.hash(value.password, saltRounds, (err, hash) => {
                        if (err){
                            res.send({
                                'success': false,
                                'msg': 'Server Error!',
                                'data': []
                            })
                        }
                        else {
                            const signupSql = 'INSERT INTO customer (email,password,phone,address) VALUES ($1, $2,$3,$4)';
                            pool.query(signupSql,[value.email,hash,value.phone,value.address],(err,result)=>{
                                if (err){
                                    console.log(err);
                                    res.send({
                                        'success': false,
                                        'msg': 'Server Error!,xx',
                                        'data': []
                                    })
                                }
                                else {
                                    res.send({
                                        'success': true,
                                        'msg': 'Signup success',
                                        'data': value.email
                                    })
                                }
                            })
                        }
                    })
                }
            }
        })
        
    }
})

module.exports = router;
