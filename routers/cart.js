const express = require('express');
var router = express.Router();

const { Pool } = require('pg');
const dotenv = require('dotenv');
const { check, validationResult } = require('express-validator');

dotenv.config();

const pool = new Pool({
    connectionString: process.env.DATABASE_URL
});

// CART
router.get('/getCurrentCartByClientID', (request, response) => {
    const client_id = parseInt(request.query.client_id)

    const queryText = `SELECT * FROM cart 
                        WHERE id = (select max(id) from 
                        (SELECT * FROM cart where client_id = $1) as foo)`

    pool.query(queryText, [client_id], (error, results) => {
        if (error) {
            throw error
        }
        response.status(200).json(results.rows)
    })
})

// history
router.get('/getAllCartByClientID', (request, response) => {
    const client_id = parseInt(request.query.client_id)
    pool.query('SELECT * FROM cart WHERE client_id = $1', [client_id], (error, results) => {
        if (error) {
            throw error
        }
        response.status(200).json(results.rows)
    })
})

// create cart 
router.post('/createCart', (request, response) => {
    const { client_id } = request.body
    pool.query('INSERT INTO cart (client_id) VALUES ($1)', [client_id], (error, results) => {
        if (error) {
            throw error
        }
        console.log(results)
        response.status(201).send(`Created cart`)
    })
})

// click for cart
router.put('/updateCart', (request, response) => {
    const { client_id, client_name, address, phone, total } = request.body

    const queryText = `UPDATE cart
	                    SET client_name = $2, address = $3, phone = $4, total = $5
                        WHERE id = (SELECT id FROM cart
                        WHERE id = (select max(id) from 
                        (SELECT * FROM cart where client_id = $1) as foo))`

    pool.query(queryText, [client_id, client_name, address, phone, total], (error, results) => {
        if (error) {
            throw error
        }
        console.log(results)
        response.status(200).json({"result":"aaa"})
    })
})

// cart_product
router.post('/addProductToCart', (request, response) => {
    const product_id = request.body.product_id
    const client_id = request.body.client_id
    const quantity = request.body.quantity

    pool.query(`INSERT INTO cart_product (product_id, cart_id, quantity) 
            VALUES ($1, (SELECT id FROM cart
            WHERE id = (select max(id) from 
            (SELECT * FROM cart where client_id = $2) as foo)),$3)`, [product_id, client_id, quantity], (error, results) => {
        if (error) {
            throw error
        }
        response.status(200).json({"result":"aaa"})
    })
})


router.delete('/deleteProductFromCart', (request, response) => {
    const product_id = parseInt(request.body.product_id)
    const client_id = parseInt(request.body.client_id)

    pool.query(`DELETE FROM cart_product 
                WHERE product_id = $1 AND cart_id = (SELECT id FROM cart
                     WHERE id = (select max(id) from 
                    (SELECT * FROM cart where client_id = $2) as foo));`, [product_id, client_id], (error, results) => {
        if (error) {
            throw error
        }
        response.status(200).json({"result":"aaa"})
    })
})

router.put('/updateQuantityProductInCart', (request, response) => {
    const { quantity, product_id, client_id } = request.body

    pool.query(`UPDATE cart_product
                SET quantity = $1 WHERE product_id = $2 
                    AND cart_id = (SELECT id FROM cart
                        WHERE id = (select max(id) from 
                        (SELECT * FROM cart where client_id = $3) as foo))`, [quantity, product_id, client_id], (error, results) => {
        if (error) {
            throw error
        }
        response.status(200).json({"result":"aaa"})
    })
})


router.get('/getAllProductByCartID', (request, response) => {
    const client_id = parseInt(request.query.client_id);
    pool.query(`SELECT 
    cart_product.cart_id, cart_product.product_id, cart_product.quantity,
    product.product_name, product.price
    FROM 
    cart_product
    INNER JOIN 
    product
    ON
    cart_product.product_id = product.id 
    WHERE cart_product.cart_id = (select max(id) from 
                            (SELECT * FROM cart where client_id = $1) as foo)`, [client_id], (error, results) => {
        if (error) {
            throw error
        }
        response.status(200).json(results.rows)
    })
})

router.post('/getProductByProductID', (request, response) => { 
    const product_id = request.body.product_id
    const client_id =  request.body.client_id
    pool.query(`SELECT * FROM cart_product WHERE product_id = $1 AND
    cart_id = (select max(id) from 
    (SELECT * FROM cart where client_id = $2) as foo)
    `,[product_id, client_id] ,(error, results) => {
        if (error) {
          throw error
        }
        response.status(200).json(results.rows)
    })
  })


module.exports = router;